#! /bin/sh

cd /fut_src/src_fedit
tar -xzf fedit2-0.0.1.tar.gz
rm fedit2-0.0.1.tar.gz
mv fedit2-0.0.1 ../fedit2
cd ../fedit2

./configure
make
make install

# https://herodrigues.me/robocup2d-tutorial/md_sections_formations-with-fedit.html
ldconfig -v
