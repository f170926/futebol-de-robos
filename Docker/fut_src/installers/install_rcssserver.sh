#! /bin/sh

cd /fut_src/src_server
tar -xzf rcssserver-15.2.2.tar.gz
rm rcssserver-15.2.2.tar.gz
mv rcssserver-15.2.2 ../server
cd ../server


# A explicação das flags vem da seguinte Issue:
# https://github.com/rcsoccersim/rcssserver/issues/1
./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/


make
make install
